package interfaceactivity;

public class LotsOfShapes {

    public static void main(String[] args) {
        Shape[] shapes = new Shape[5];
        shapes[0] = new Rectangle(2,4);
        shapes[1] = new Rectangle(8,4);
        shapes[2] = new Circle(3);
        shapes[3] = new Circle(6);
        shapes[4] = new Square(10);

        for (int i = 0; i < shapes.length; i++){
            System.out.println("Area: " + shapes[i].getArea() + ", perimeter: " + shapes[i].getPerimeter());
        }

    } 
    
}
