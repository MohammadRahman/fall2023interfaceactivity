package interfaceactivity;

public class Rectangle implements Shape {

    private double length;

    private double width;

    public Rectangle(double length, double width){
        this.length = length;
        this.width = width;
    }

    public double getLength(){
        return this.length;
    }

    public double getWidth(){
        return this.width;
    }

    public double getArea(){
        double result = this.length * this.width;
        return result;

    }

    public double getPerimeter(){
        double result = 2 * (this.length + this.width);
        return result;
    }
    
}
