package interfaceactivity;

public class Circle implements Shape{

    private double radius;

    public Circle (double radius){
        this.radius = radius;
    }

    public double getRadius(){
        return this.radius;
    }

    @Override
    public double getArea(){
        double result = Math.PI * (this.radius * this.radius);
        return result;
    }

    @Override
    public double getPerimeter(){
        double result = 2 * Math.PI * this.radius;
        return result;
    }
}

