package interfaceactivity;

public interface Shape {
    double getArea();
    double getPerimeter();
    
}
